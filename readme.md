# PHP 1

Första veckan av PHP kommer vi att gå igenom hur man inkluderar andra filer med hjälp av include function. 

### Steg 1

Skapa en ny mapp som heter ex php1.

Skapa tre filer som heter index.php, header.php och footer.php

i header.php ska ni lägga in eran meny samt eran header som vi gjorde när vi jobbade med html och css.

Till exmpel:

```html

<html>
<head>
	<title>Min sida </title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="theme.css">
<head
<header>
<nav> Min meny</nav>
</header>
<body>

```

I footer.php ska ni lägga in slut taggar för body och html samt ev javascipt filer

Till exmpel:

```html

<script src="myscripts.js"></script>
</body>
</html>

```

### Steg 2

I index.php ska vi inkludera filerna header.php och footer.php med hjälp av phps function include. 

Högst upp i index.php ska vi ha våran header.php fil för där ligger våran start kod för html samt våran meny och öpppnings taggen för body.

Längst ner på sidan ska vi ha våran footer.php fil för där ligger våra slut taggar.

Så här ska det se ut:

```php
// index.php

include "header.php";



include "footer.php";
```

### Steg 3

Nu är frågan varför är detta bra att använda. Jo, om vi tar menyn till exmpel. Vi har två sidor om heter index.html och ommig.html som har samma meny i sig. Och nu vill vi göra en till sida som heter minaspel.html. Och måste ni koperai all kod ifrån ex index.html till minaspel.html samt lägga in länknen till minaspel.html i menyn i alla tre sidor. 

Nu när vi använder oss av php och include functionen behöver vi bara includa header.php och footer.php samt lägga in länken till minaspel.php i menyn i header.php så kommer den länken att finnas alla tre sidor. 

Och sedan kan börja bygga sidan minaspel.php. 

Så här kan det se ut:

```php
//minaspel.php

include "header.php";

<h1>Mina spel</h1>
<ul>
<li> Spel 1</li>
<li> Spel 2</li>
</ul>

include "footer.php";

``` 

